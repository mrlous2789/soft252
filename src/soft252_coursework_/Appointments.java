/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252_coursework_;

/**
 *
 * @author Nick
 */
public class Appointments implements java.io.Serializable {
    private int appointmentID;
    private int doctorID;
    private int patientID;
    private String dNotes;
    private boolean approved;
    
    
    public Appointments()
    {
        
    }
    public Appointments(int inID,int dId, int pId, String notes, boolean approved)
    {
        appointmentID = inID;
        doctorID = dId;
        patientID = pId;
        dNotes = notes;
        this.approved  = approved;
    }

    public int getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(int appointmentID) {
        this.appointmentID = appointmentID;
    }   
    
    public int getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(int doctorID) {
        this.doctorID = doctorID;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public String getdNotes() {
        return dNotes;
    }

    public void setdNotes(String dNotes) {
        this.dNotes = dNotes;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
    
    
}
