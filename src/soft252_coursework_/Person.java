
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252_coursework_;

/**
 *
 * @author Nick
 */
public class Person implements java.io.Serializable {
    int admin = 0;
    int doctor = 1;
    int secretary = 2;
    int patient = 3;
    
    
    private int ID;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private int age;
    private int role;
    private String gender;
    
    public Person(int idIn, String firstNameIn, String lastNameIn, String usernameIn, String passwordIn,String genderIn, int ageIn, int roleIn)
    {
        role = roleIn;
        ID = idIn;
        firstName = firstNameIn;
        lastName = lastNameIn;
        age = ageIn;
        password = passwordIn;
        username = usernameIn;
        gender = genderIn;
    }
 

    public Person()
    {
        
    }
    //getters and setters for variables
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int type) {
        this.role = type;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
}
