/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252_coursework_;
import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
/**
 *
 * @author Nick
 */
public class Data_Manager  implements java.io.Serializable{

    Person[] accounts = new Person[0]; //accounts
    Person currentAccount; //current account
    String userFile = "users.ser"; //user filename
    Appointments[] appointments = new Appointments[0];//appointments
    String appFile = "appointments.ser";// appointment file
    
    public void Data_Manager()
    {
        
    }
    public void EncodeUsers()
    {
        try{
            FileOutputStream fos = new FileOutputStream(new File(userFile)); 
            ObjectOutputStream oos = new ObjectOutputStream(fos);        
            
            oos.writeObject(accounts);//try writing account serialized to fie
            oos.close();
            fos.close();
        }
        catch(IOException ex){ //catching expection thrown by wirting object
            System.out.println("IOException is caught");
            ex.printStackTrace();
        }
        
    }
    public void DecodeUsers()
    {        
        try{
            FileInputStream fis = new FileInputStream(new File(userFile));
            ObjectInputStream ois = new ObjectInputStream(fis);

            accounts = (Person[])ois.readObject();//read serialized object from file
            
            ois.close();
            fis.close();

        }
        catch(IOException ex)//cathich exception thrown by reading object
        {
            System.out.println("IOException is caught");
            ex.printStackTrace();
        }
        catch(ClassNotFoundException ex)
        {
           System.out.println("ClassNotFoundException is caught");
        }
    }
    
    public void FirstTimeUsers()
    {
        File tempUser = new File(userFile);
        boolean userExists = tempUser.exists();
        File tempApp = new File(appFile);
        boolean appExists = tempApp.exists();
        if(!userExists)
        {
            this.accounts = defaultPeople();
            EncodeUsers();
        }
        if(!appExists)
        {
            this.appointments = defaultAppointments();
            EncodeAppointments();
        }
    }
    
    
    public Person[] defaultPeople() // default accounts required for coursework
    {
        Person[] tempPeople = new Person[8];
        tempPeople[0] = new Person(1001,"John","Smith","admin","admin","Male", 34, 0);
        tempPeople[1] = new Person(1002,"Dylan","Palmer","doctor1","doctor1","Male", 47, 1);
        tempPeople[2] = new Person(1003,"Reggie","Fox","doctor2","doctor2","Male", 40, 1);
        tempPeople[3] = new Person(1004,"Teddy","Morgan","doctor3","doctor3","Male", 28, 1);
        tempPeople[4] = new Person(1005,"Bradley","Simpson","secretary","secretary","Male", 65, 2);
        tempPeople[5] = new Person(1006,"Aidan","Stewart","patient1","patient1","Male", 40, 3);
        tempPeople[6] = new Person(1007,"Frank","Ross","patient2","patient2","Male", 18, 3);
        tempPeople[7] = new Person(1008,"Erik","Collins","patient3","patient3","Male", 54, 3);
        return tempPeople;
    }
    
    public Object[] GetAccountData(int i) //return account a pos i in accounts[] as an object, used for the table in ManageAccountsAdmin
    {
        String tempRole = "";
        switch(accounts[i].getRole())
        {
            case 0:
                tempRole = "Admin";
                break;
            case 1:
                tempRole = "Doctor";
                break;
            case 2:
                tempRole = "Secretary";
                break;
            case 3:
                tempRole = "Patient";
                break;
        }

        Object[] ad = {accounts[i].getID(),accounts[i].getFirstName(),accounts[i].getLastName(),accounts[i].getUsername(),accounts[i].getPassword(),accounts[i].getAge(),accounts[i].getGender(),tempRole};
        return ad;
                
    }
    
    public int generateUserID()//generate unique id by sorting current ids than adding one the the biggest id num
    {
        int tempId;
        boolean sorted = false;
        
        int[] idArray = new int[this.accounts.length];
        
        for (int i = 0; i < this.accounts.length; i++) {
            idArray[i] = this.accounts[i].getID();
        }
        while(sorted == false)
        {
            sorted = true;
            for (int i = 0; i < idArray.length - 1; i++) {
                if (idArray[i] > idArray[i + 1]) {
                    sorted = false;
                    int tempInt = idArray[i + 1];
                    idArray[i + 1] = idArray[i];
                    idArray[i] = tempInt;
                }
            }
        }
        tempId = idArray[idArray.length - 1] + 1;
        
        return tempId;
    }
    
    public int GenerateAppointmentID()
    {
        int[] appIDs = new int[this.appointments.length];
        int newID = 0;
        boolean sorted = false;
        for (int i = 0; i < this.appointments.length; i++) {
            appIDs[i] = this.appointments[i].getAppointmentID();
        }
        
        while(sorted == false)
        {
            sorted = true;
            for (int i = 0; i < appIDs.length - 1; i++) {
                if (appIDs[i] > appIDs[i + 1]) {
                    sorted = false;
                    int tempInt = appIDs[i + 1];
                    appIDs[i + 1] = appIDs[i];
                    appIDs[i] = tempInt;
                }
            }
        }
        newID = appIDs[appIDs.length - 1] + 1;
        return newID;        
    }

    public void EncodeAppointments()
    {
        try{
            FileOutputStream fos = new FileOutputStream(new File(appFile)); 
            ObjectOutputStream oos = new ObjectOutputStream(fos);        
            
            oos.writeObject(appointments);//try writing account serialized to fie
            oos.close();
            fos.close();
        }
        catch(IOException ex){ //catching expection thrown by wirting object
            System.out.println("IOException is caught");
            ex.printStackTrace();
        }
    }
    public void DecodeAppointments()
    {
        try{
            FileInputStream fis = new FileInputStream(new File(appFile));
            ObjectInputStream ois = new ObjectInputStream(fis);

            appointments = (Appointments[])ois.readObject();//read serialized object from file
            
            ois.close();
            fis.close();

        }
        catch(IOException ex)//cathich exception thrown by reading object
        {
            System.out.println("IOException is caught");
            ex.printStackTrace();
        }
        catch(ClassNotFoundException ex)
        {
           System.out.println("ClassNotFoundException is caught");
        }        
    }

    private Appointments[] defaultAppointments() //some starting appointments
    {
        Appointments[] tempApp = new Appointments[3];
        tempApp[0] = new Appointments(1,1002, 1006,"Cough",true);
        tempApp[1] = new Appointments(2,1003,1007,"Nothing wrong with them",true);
        tempApp[2] = new Appointments(3,1004,1008,"Ankle sprained",true);
        return tempApp;
    }
    
    public Person accountByID(int id)
    {
        Person tempPerson = new Person();
        for (int i = 0; i < this.accounts.length; i++) {
            if(accounts[i].getID() == id)
            {
                return accounts[i];
            }
        }
        return tempPerson;
    }
    
    public void DecodeData()
    {
        DecodeUsers();
        DecodeAppointments();
    }
    public void EncodeData()
    {
        EncodeUsers();
        EncodeAppointments();
    }
}

